import os

from flask import Flask, render_template

from views.simplearmory import simplearmory_blueprint
from views.spec_ratings import spec_ratings_blueprint

app = Flask(__name__)
app.secret_key = os.environ.get('SECRET_KEY')
app.config.update(
    ADMIN=os.environ.get('ADMIN')
)

app.register_blueprint(spec_ratings_blueprint, url_prefix='/spec-ratings')
app.register_blueprint(simplearmory_blueprint, url_prefix='/simplearmory')


@app.route('/')
def index():
    return render_template('index.html')


if __name__ == '__main__':
    app.run()
