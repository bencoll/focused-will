import json
from json import JSONDecodeError

from flask import Blueprint, render_template, request, flash, session

spec_ratings_blueprint = Blueprint('spec_ratings', __name__)

wow_classes = [
    ('deathknight', 'blood,frost,unholy'),
    ('demonhunter', 'havoc,vengence'),
    ('druid', 'balance,feral,guardian,restoration'),
    ('hunter', 'beastmastery,marksmanship,survival'),
    ('mage', 'arcane,fire,frost'),
    ('monk', 'brewmaster,mistweaver,windwalker'),
    ('paladin', 'holy,protection,retribution'),
    ('priest', 'discipline,holy,shadow'),
    ('rogue', 'assassination,outlaw,subtlety'),
    ('shaman', 'elemental,enhancement,restoration'),
    ('warlock', 'affliction,demonology,destruction'),
    ('warrior', 'arms,fury,protection')
]


@spec_ratings_blueprint.route('/', methods=['GET'])
def index():
    return render_template('ratings/spec_ratings.html')


@spec_ratings_blueprint.route('/load-ratings', methods=['GET', 'POST'])
def load_ratings():
    if request.method == 'POST':
        ratings_json = request.form.get('spec-ratings')
        if ratings_json != "":
            try:
                ratings = json.loads(ratings_json)
                session["ratings"] = ratings
                return render_template('ratings/spec_ratings.html')
            except JSONDecodeError:
                flash("The JSON you have input is invalid.", 'danger')
    return render_template('ratings/load_ratings.html')


@spec_ratings_blueprint.route('/save-ratings', methods=['POST'])
def save_ratings():
    ratings_dict = {}
    for wow_class, specs in wow_classes:
        ratings_dict[wow_class] = {}
        for spec in specs.split(','):
            spec_string = f'{wow_class}-{spec}'
            ratings_dict[wow_class][spec] = request.form.get(spec_string)
        ratings_dict[wow_class]['comments'] = request.form.get(f'{wow_class}-comments')
    ratings_json = json.dumps(ratings_dict, indent=4)
    session['ratings'] = ratings_dict
    return render_template('ratings/save_ratings.html', ratings_json=ratings_json)


@spec_ratings_blueprint.route('/tiermaker', methods=['GET'])
def tiermaker():
    return render_template('ratings/tiermaker.html')
