from flask import Blueprint, render_template

simplearmory_blueprint = Blueprint('simeplearmory', __name__)


@simplearmory_blueprint.route('/', methods=['GET'])
def index():
    return render_template('simplearmory/mounts.html')
